import matplotlib.pyplot as plt
import sys

HIST_NAMES = ['bmp', 'bwt', 'diff', 'pred']
# one_window = False
# if len(sys.argv) > 1 and sys.argv[1] == 'one-window':
#     one_window = True


def read_values(file_name):
    val = None
    try:
        file = open('../../log/' + file_name + '_hist.txt', 'r')
        val = [float(i) for i in file.readlines()]
        file.close()
    except FileNotFoundError:
        print("File for " + file_name + " not found.")
    return val


values = [read_values(file_name) for file_name in HIST_NAMES]
for index, hist in enumerate(values):
    if not hist:
        continue
    else:
        plt.figure()
    plt.plot(hist)
    plt.ylabel(HIST_NAMES[index].upper())
plt.show()
