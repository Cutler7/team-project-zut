from PIL import Image

pixels = []

file = open('../../log/pixels.csv', 'r')
for row in file.readlines():
    elements = row.split(',')
    elements.pop()
    pixels.append([int(i) for i in elements])
file.close()

pixels.reverse()
width = len(pixels[0])
height = len(pixels)
print('SIZE: ', width, 'x', height)

im = Image.new('L', (width, height))
for i, row in enumerate(pixels):
    for j, pixel in enumerate(row):
        im.putpixel((j, i), pixel)

im.show()
im.save('test.bmp', 'BMP')
