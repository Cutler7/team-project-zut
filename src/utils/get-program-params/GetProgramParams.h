//
// Created by Daniel on 05.05.2020.
//

#define NUMBER_OF_STEPS 5

#ifndef TEAM_PROJECT_ZUT_GETPROGRAMPARAMS_H
#define TEAM_PROJECT_ZUT_GETPROGRAMPARAMS_H

#include <string>

using namespace std;

enum ImageOperation {
    EMPTY,
    COLUMN_BWT,
    ROW_BWT,
    FULL_BWT,
    DIFF_CODING,
    PREDICT_CODING,
    MTF
};

typedef struct registeredParams {
    bool saveLogs{};
    bool saveCsv{};
    string fileName{};
    ImageOperation steps[NUMBER_OF_STEPS]{};
} RegisteredParams;

RegisteredParams getProgramParams(int argc, char *argv[]);

void assignParams(int opt, int opt_index, RegisteredParams &params);

void addStep(int stepNo, const string &value, RegisteredParams &params);

#endif //TEAM_PROJECT_ZUT_GETPROGRAMPARAMS_H
