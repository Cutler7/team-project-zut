//
// Created by Daniel on 05.05.2020.
//

#include "GetProgramParams.h"
#include <getopt.h>
#include <iostream>
#include <map>

using namespace std;

RegisteredParams getProgramParams(int argc, char **argv) {
    if (argc == 1) throw runtime_error("No parameters passed");
    RegisteredParams result;
    result.fileName = argv[argc - 1];
    int opt{};
    int option_index = 0;
    struct option long_options[] = {
            {"step1", required_argument, nullptr, 0},
            {"step2", required_argument, nullptr, 0},
            {"step3", required_argument, nullptr, 0},
            {"step4", required_argument, nullptr, 0},
            {"step5", required_argument, nullptr, 0},
            {nullptr, 0,                 nullptr, 0}
    };

    while (opt != -1) {
        opt = getopt_long(argc, argv, "SsCc", long_options, &option_index);
        assignParams(opt, option_index, result);
    }

    return result;
}

void assignParams(int opt, int opt_index, RegisteredParams &params) {
    switch (opt) {
        case 0:
            addStep(opt_index, optarg, params);
            break;
        case -1:
            break;
        case 's':
        case 'S':
            params.saveLogs = true;
            break;
        case 'c':
        case 'C':
            params.saveCsv = true;
            break;
        case '?':
            cout << "Unrecognized option" << endl;
            break;
        default:
            throw runtime_error("Command line params could not be parsed");
    }
}

void addStep(int stepNo, const string &value, RegisteredParams &params) {
    map<string, ImageOperation> paramOperationMap = {
            {"col-bwt",  COLUMN_BWT},
            {"row-bwt",  ROW_BWT},
            {"full-bwt", FULL_BWT},
            {"diff",     DIFF_CODING},
            {"pred",     PREDICT_CODING},
            {"mtf",      MTF},
    };
    params.steps[stepNo] = paramOperationMap.at(value);
}
