//
// Created by Daniel on 08.05.2020.
//

#include <iostream>
#include <Bwt.h>
#include <DiffBmp.h>
#include <PredBmp.h>

using namespace std;

ImageData *runTransformation(ImageData *source, ImageData *result, ImageOperation transformType, const RegisteredParams &params) {
    switch (transformType) {
        case ImageOperation::EMPTY:
            return source;
        case ImageOperation::COLUMN_BWT:
            result = new Bwt(source, BwtVariant::COL, params);
            break;
        case ImageOperation::ROW_BWT:
            result = new Bwt(source, BwtVariant::ROW, params);
            break;
        case ImageOperation::FULL_BWT:
            result = new Bwt(source, BwtVariant::FULL, params);
            break;
        case ImageOperation::DIFF_CODING:
            result = new DiffBmp(source, params);
            break;
        case ImageOperation::PREDICT_CODING:
            result = new PredBmp(source, params);
            break;
        case ImageOperation::MTF:
            result = source;
            cout<<"Option not implemented"<<endl;
            break;
        default:
            throw runtime_error("No such file in this path");
    }
//    delete source;
    return result;
}
