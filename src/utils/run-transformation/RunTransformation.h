//
// Created by Daniel on 08.05.2020.
//

#ifndef TEAM_PROJECT_ZUT_RUNTRANSFORMATION_H
#define TEAM_PROJECT_ZUT_RUNTRANSFORMATION_H

#include "ImageData.h"
#include "GetProgramParams.h"

ImageData *runTransformation(ImageData *source, ImageData *result, ImageOperation transformType, const RegisteredParams &params);

#endif //TEAM_PROJECT_ZUT_RUNTRANSFORMATION_H
