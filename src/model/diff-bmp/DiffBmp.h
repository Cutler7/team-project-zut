//
// Created by cutler on 02.04.2020.
//

#ifndef TEAM_PROJECT_ZUT_DIFFBMP_H
#define TEAM_PROJECT_ZUT_DIFFBMP_H


#include <stdint-gcc.h>
#include "Bmp.h"
#include "ImageData.h"

class DiffBmp : public ImageData {

public:
    DiffBmp(ImageData *bmp, const RegisteredParams &params);

private:
    void codeDifferential(ImageData *bmp);
};

#endif //TEAM_PROJECT_ZUT_DIFFBMP_H
