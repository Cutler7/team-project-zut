//
// Created by cutler on 02.04.2020.
//

#include "DiffBmp.h"
#include <cmath>

using namespace std;

DiffBmp::DiffBmp(ImageData *bmp, const RegisteredParams &params) : ImageData(params) {
    this->H_RES = 510;
    this->H_OFFSET = 255;
    this->fileName = "diff";
    this->width = bmp->getWidth();
    this->height = bmp->getHeight();
    this->codeDifferential(bmp);
}

void DiffBmp::codeDifferential(ImageData *bmp) {
    this->pixelData = new short*[this->height];
    for (int i=0; i<height; i++) {
        this->pixelData[i] = new short[width];
    }
    this->pixelData[0][0] = bmp->getPixels()[0][0];
    for (int i=1; i<height; i++) {
        this->pixelData[i][0] = bmp->getPixels()[i][0] - bmp->getPixels()[i - 1][0];
    }
    for (int i=0; i< height; i++) {
        for (int j=1; j < width; j++) {
            this->pixelData[i][j] = bmp->getPixels()[i][j] - bmp->getPixels()[i][j - 1];
        }
    }
}
