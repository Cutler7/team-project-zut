//
// Created by cutler on 15.04.2020.
//

#include "Bwt.h"
#include <algorithm>

using namespace std;

bool comp(short *a, short *b) {
    return a[0] < b[0];
}

Bwt::Bwt(ImageData *bmp, BwtVariant variant, const RegisteredParams &params) : ImageData(params) {
    this->H_RES = bmp->getHRes();
    this->H_OFFSET = bmp->getHOffset();
    this->fileName = "bwt";
    this->width = bmp->getWidth();
    this->height = bmp->getHeight();
    this->prepareBuf();
    this->pixelData = bmp->getPixels();
    this->matrixBwt(this->getPixels(), variant);
}

Bwt::~Bwt() {
    for (int i = 0; i < this->width; i++) {
        delete[] this->rBuf[i];
    }
    for (int i = 0; i < this->height; i++) {
        delete[] this->cBuf[i];
    }
    delete[] this->rBuf;
    delete[] this->cBuf;
}

void Bwt::matrixBwt(short **matrix, BwtVariant variant) {
    if (variant == BwtVariant::ROW || variant == BwtVariant::FULL) {
        for (int i = 0; i < this->height; i++) {
            this->rowVectBwt(matrix, i);
        }
    }
    if (variant == BwtVariant::COL || variant == BwtVariant::FULL) {
        for (int i = 0; i < this->width; i++) {
            this->colVectBwt(matrix, i);
        }
    }
}

void Bwt::rowVectBwt(short **matrix, int rowNo) {
    int len = this->getWidth();
    for (int i = 0; i < len; i++) {
        this->rBuf[i][0] = matrix[rowNo][(i + 1) % len];
        this->rBuf[i][1] = matrix[rowNo][(i) % len];
    }
    sort(this->rBuf, this->rBuf + len, comp);
    for (int i = 0; i < len; i++) {
        this->pixelData[rowNo][i] = this->rBuf[i][1];
    }
}

void Bwt::colVectBwt(short **matrix, int colNo) {
    int len = this->getHeight();
    for (int i = 0; i < len; i++) {
        this->cBuf[i][0] = matrix[(i + 1) % len][colNo];
        this->cBuf[i][1] = matrix[(i) % len][colNo];
    }
    sort(this->cBuf, this->cBuf + len, comp);
    for (int i = 0; i < len; i++) {
        this->pixelData[i][colNo] = this->cBuf[i][1];
    }
}

void Bwt::prepareBuf() {
    this->pixelData = new short *[this->height];
    this->rBuf = new short *[this->width];
    this->cBuf = new short *[this->height];
    for (int i = 0; i < this->width; i++) {
        this->rBuf[i] = new short[2];
    }
    for (int i = 0; i < this->height; i++) {
        this->pixelData[i] = new short[this->width];
        this->cBuf[i] = new short[2];
    }
}
