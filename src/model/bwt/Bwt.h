//
// Created by cutler on 15.04.2020.
//

#ifndef TEAM_PROJECT_ZUT_BWT_H
#define TEAM_PROJECT_ZUT_BWT_H


#include <cstdint>
#include "ImageData.h"

class Bwt : public ImageData {

    short **rBuf;
    short **cBuf;

public:
    Bwt(ImageData *bmp, BwtVariant variant, const RegisteredParams &params);
    ~Bwt();

private:
    void rowVectBwt(short **matrix, int rowNo);
    void colVectBwt(short **matrix, int colNo);
    void matrixBwt(short **matrix, BwtVariant variant);
    void prepareBuf();
};

#endif //TEAM_PROJECT_ZUT_BWT_H
