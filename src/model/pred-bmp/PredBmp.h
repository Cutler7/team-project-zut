//
// Created by Daniel on 05.05.2020.
//

#ifndef TEAM_PROJECT_ZUT_PREDBMP_H
#define TEAM_PROJECT_ZUT_PREDBMP_H

#include "ImageData.h"
#include "Bmp.h"
#include <Eigen/Dense>

#define PRED 3

class PredBmp : public ImageData {

public:
    PredBmp(ImageData *bmp, const RegisteredParams &params);

private:
    Eigen::Matrix<double, PRED, PRED> R{};
    Eigen::Matrix<double, PRED, 1> P{};
    Eigen::Matrix<double, PRED, 1> coVect{};

    short **pixelDataCpy{};
    void codePred(ImageData *bmp);
    void calcMatrix();
    void calcVector();
    short calcExpValue(int x, int y);
    short calcBorder(ImageData *bmp);
    long getMatrixElement(int m, int n);
    short getNeighborVal(int x, int y, int num);
};

#endif //TEAM_PROJECT_ZUT_PREDBMP_H
