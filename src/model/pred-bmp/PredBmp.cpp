//
// Created by Daniel on 05.05.2020.
//

#include <iostream>
#include <math.h>
#include "PredBmp.h"

using namespace std;

PredBmp::PredBmp(ImageData *bmp, const RegisteredParams &params) : ImageData(params) {
    this->fileName = "pred";
    this->H_RES = 510;
    this->H_OFFSET = 255;
    this->width = bmp->getWidth();
    this->height = bmp->getHeight();
    this->codePred(bmp);
}

void PredBmp::codePred(ImageData *bmp) {
    this->pixelDataCpy = bmp->getPixels();
    this->calcMatrix();
    this->calcVector();
    this->coVect = this->R.inverse() * this->P;
//    cout << coVect << endl;
//    cout << coVect(0, 0) + coVect(1, 0) + coVect(2, 0) << endl;
    this->pixelData = new short *[this->height];
    this->pixelData[0] = new short[width];

    for (int i = 1; i < this->getHeight(); i++) {
        this->pixelData[i] = new short[width];
        for (int j = 1; j < this->getWidth(); j++) {
            short expValue = this->calcExpValue(i, j);
            if (expValue > 255) {
                expValue = 255;
            }
            this->pixelData[i][j] = (short) (this->pixelDataCpy[i][j] - expValue);
        }
    }
    this->calcBorder(bmp);
}

short PredBmp::calcExpValue(int x, int y) {
    double expValue = 0;
    for (int i = 0; i < PRED; i++) {
        expValue += this->coVect(i, 0) * this->getNeighborVal(x, y, i + 1);
    }
    return round(expValue);
}

void inline PredBmp::calcMatrix() {
    for (int i = 0; i < this->R.rows(); i++) {
        for (int j = 0; j < this->R.cols(); j++) {
            this->R(i, j) = this->getMatrixElement(i, j);
        }
    }
}

void inline PredBmp::calcVector() {
    for (int i = 0; i < this->P.rows(); i++) {
        this->P(i, 0) = this->getMatrixElement(i, -1);
    }
}

long PredBmp::getMatrixElement(int m, int n) {
    long sum = 0;
    for (int i = 1; i < this->getHeight(); i++) {
        for (int j = 1; j < this->getWidth(); j++) {
            sum += this->getNeighborVal(i, j, m + 1) * this->getNeighborVal(i, j, n + 1);
        }
    }
    return sum;
}

short PredBmp::getNeighborVal(int x, int y, int num) {
    switch (num) {
        case 0:
            return this->pixelDataCpy[x][y];
        case 1:
            return this->pixelDataCpy[x][y - 1];
        case 2:
            return this->pixelDataCpy[x - 1][y];
        case 3:
            return this->pixelDataCpy[x - 1][y - 1];
        default:
            return 0;
    }
}

short PredBmp::calcBorder(ImageData *bmp) {
    this->pixelData[0][0] = bmp->getPixels()[0][0];
    for (int i = 1; i < this->getHeight(); i++) {
        this->pixelData[i][0] = bmp->getPixels()[i][0] - bmp->getPixels()[i - 1][0];
    }
    for (int i = 1; i < this->getWidth(); i++) {
        this->pixelData[0][i] = bmp->getPixels()[0][i] - bmp->getPixels()[0][i - 1];
    }
}
