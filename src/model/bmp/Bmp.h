//
// Created by cutler on 01.04.2020.
//

#ifndef TEAM_PROJECT_ZUT_BMP_H
#define TEAM_PROJECT_ZUT_BMP_H


#include <cstdint>
#include <string>
#include <vector>
#include "ImageData.h"

using namespace std;

struct BmpFileHeader {
    uint16_t file_type{0};
    uint32_t file_size{0};
    uint16_t reserved1{0};
    uint16_t reserved2{0};
    uint32_t offset_data{0};
};

struct BmpInfoHeader {
    uint32_t size{0};
    int32_t width{0};
    int32_t height{0};
    uint16_t planes{0};
    uint16_t bit_count{0};
    uint32_t compression{0};
    uint32_t size_image{0};
    int32_t x_pixels_per_meter{0};
    int32_t y_pixels_per_meter{0};
    uint32_t colors_used{0};
    uint32_t colors_important{0};
};

class Bmp : public ImageData {

    BmpFileHeader file_header;
    BmpInfoHeader info_header;

public:
    Bmp(const string &fileLocation, const RegisteredParams &params);

private:
    void readFile(const string &fileLocation);
    void readFileHeader(ifstream& file);
    void readInfoHeader(ifstream& file);
    void readPixelData(ifstream& file);
};


#endif //TEAM_PROJECT_ZUT_BMP_H
