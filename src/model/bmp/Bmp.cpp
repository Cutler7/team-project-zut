//
// Created by cutler on 01.04.2020.
//

#include "Bmp.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void Bmp::readFile(const string &fileLocation) {

    ifstream file(fileLocation, ios_base::binary);

    if (!file) {
        throw runtime_error("No such file in this path");
    };

    this->readFileHeader(file);

    if (file_header.file_type != 0x4D42) {
        throw runtime_error("Wrong file format");
    }

    this->readInfoHeader(file);
    this->readPixelData(file);
    file.close();
}

void Bmp::readFileHeader(ifstream &file) {
    file.read((char *) &(this->file_header.file_type), sizeof(uint16_t));
    file.read((char *) &(this->file_header.file_size), sizeof(uint32_t));
    file.read((char *) &(this->file_header.reserved1), sizeof(uint16_t));
    file.read((char *) &(this->file_header.reserved2), sizeof(uint16_t));
    file.read((char *) &(this->file_header.offset_data), sizeof(uint32_t));
}

void Bmp::readInfoHeader(ifstream &file) {
    file.read((char *) &(this->info_header.size), sizeof(uint32_t));
    file.read((char *) &(this->info_header.width), sizeof(int32_t));
    file.read((char *) &(this->info_header.height), sizeof(int32_t));
    file.read((char *) &(this->info_header.planes), sizeof(uint16_t));
    file.read((char *) &(this->info_header.bit_count), sizeof(uint16_t));
    file.read((char *) &(this->info_header.compression), sizeof(uint32_t));
    file.read((char *) &(this->info_header.size_image), sizeof(uint32_t));
    file.read((char *) &(this->info_header.x_pixels_per_meter), sizeof(uint32_t));
    file.read((char *) &(this->info_header.y_pixels_per_meter), sizeof(uint32_t));
    file.read((char *) &(this->info_header.colors_used), sizeof(uint32_t));
    file.read((char *) &(this->info_header.colors_important), sizeof(uint32_t));
}

void Bmp::readPixelData(ifstream &file) {
    file.seekg(file_header.offset_data, ifstream::beg);
    int width = this->info_header.width;
    int height = this->info_header.height;
    int paddingOffset = width % 4;
    this->pixelData = new short *[height];
    for (int i = 0; i < height; i++) {
        this->pixelData[i] = new short[width];
        for (int j = 0; j < width; j++) {
            file.read((char *) &this->pixelData[i][j], sizeof(uint8_t));
        }
        file.seekg(paddingOffset, ifstream::cur);
    }
}

Bmp::Bmp(const string &fileLocation, const RegisteredParams &params) : ImageData(params) {
    this->H_RES = 255;
    this->H_OFFSET = 0;
    this->fileName = "bmp";
    this->readFile(fileLocation);
    this->width = this->info_header.width;
    this->height = this->info_header.height;
}
