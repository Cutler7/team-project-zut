//
// Created by cutler on 02.04.2020.
//

#include "ImageData.h"
#include <cmath>
#include <iostream>
#include <fstream>

using namespace std;

double *ImageData::getImgHist() {
    int *hist = new int[H_RES];
    double *nHist = new double[H_RES];
    fill(hist, hist+H_RES, 0);
    for (int i=0; i<this->getHeight(); i++) {
        for (int j=0; j<this->getWidth(); j++) {
            int val = this->pixelData[i][j];
            hist[H_OFFSET+val]++;
        }
    }
    int elemQuantity = this->getWidth() * this->getHeight();
    for (int i=0; i<H_RES; i++) {
        nHist[i] = (double)hist[i] / (double)elemQuantity;
    }
    runAdditionals(nHist, this->getPixels());
    delete[] hist;
    return nHist;
}

double ImageData::getImgEntropy() {
    double *hist = this->getImgHist();
    double entropy = 0.0;
    for (int i=0; i<H_RES; i++) {
        entropy += hist[i] == 0 ? 0 : (hist[i] * log2(hist[i]));
    }
    entropy = entropy * (-1);
//    cout<<this->fileName<<": Entropy: "<<entropy<<endl;
    return entropy;
}

short **ImageData::getPixels() {
    return this->pixelData;
}

int ImageData::getHRes() {
    return this->H_RES;
}

int ImageData::getHOffset() {
    return this->H_OFFSET;
}

ImageData::ImageData(const RegisteredParams &params) {
    this->params = params;
}

ImageData::~ImageData() {
    for (int i = 0; i < this->height; i++) {
        delete[] this->pixelData[i];
    }
    delete[] this->pixelData;
}

int ImageData::getHeight() {
    return this->height;
}

int ImageData::getWidth() {
    return this->width;
}

double ImageData::getBitAvg(BwtVariant opt) {
    double result = this->getImgEntropy();
    double logH = log2(this->getHeight()) / this->getHeight();
    double logW = log2(this->getWidth()) / this->getWidth();
    if (opt == BwtVariant::COL) {
        result += logH;
    } else if (opt == BwtVariant::ROW) {
        result += logW;
    } else if (opt == BwtVariant::FULL) {
        result += logH + logW;
    }
    cout<<this->params.fileName<<": "<<result<<endl;
    return result;
}

void ImageData::runAdditionals(double *hist, short **pixels) {
    if (this->params.saveLogs) {
        this->saveHist(hist);
    }
    if (this->params.saveCsv) {
        this->saveCsv(pixels);
    }
}

void ImageData::saveHist(double *hist) {
    ofstream file(this->logPath + this->fileName + "_hist.txt", ofstream::trunc | ofstream::out);
    for (int i=0; i<H_RES; i++) {
        file<<hist[i]<<endl;
    }
    file.close();
}

void ImageData::saveCsv(short **pixels) {
    ofstream file(this->logPath + "pixels.csv", ofstream::trunc | ofstream::out);
    for (int i=0; i<this->getHeight(); i++) {
        for (int j=0; j<this->getWidth(); j++) {
            file<<pixels[i][j]<<",";
        }
        file<<"\n";
    }
    file.close();
}
