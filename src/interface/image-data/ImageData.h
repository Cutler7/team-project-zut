//
// Created by cutler on 02.04.2020.
//

#ifndef TEAM_PROJECT_ZUT_IMAGEDATA_H
#define TEAM_PROJECT_ZUT_IMAGEDATA_H


#include <cstdint>
#include <string>
#include <GetProgramParams.h>

using namespace std;

enum BwtVariant {
    NONE,
    ROW,
    COL,
    FULL,
};

class ImageData {

protected:
    RegisteredParams params{};
    string logPath = "../log/";
    string fileName{0};
    int width{0};
    int height{0};
    int H_RES{0};
    int H_OFFSET{0};
    short **pixelData{};

public:
    ImageData(const RegisteredParams &params);
    ~ImageData();
    int getHRes();
    int getHOffset();
    int getWidth();
    int getHeight();
    virtual short **getPixels();
    virtual double getImgEntropy();
    virtual double getBitAvg(BwtVariant opt);
    virtual double *getImgHist();

protected:
    void runAdditionals(double *hist, short **pixels);
    void saveHist(double *hist);
    void saveCsv(short **pixels);
};

#endif //TEAM_PROJECT_ZUT_IMAGEDATA_H
