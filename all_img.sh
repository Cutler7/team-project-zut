#!/bin/bash

IMAGES1=("balloon.bmp" "BARB.bmp" "BARB2.bmp" "BOARD.bmp" "boats.bmp" "GIRL.bmp" "GOLD.bmp" "HOTEL.bmp" "ZELDA.bmp")
IMAGES2=("airplane.bmp" "baboonTMW.bmp" "balloon.bmp" "BARB.bmp" "BARB2.bmp" "camera256.bmp" "couple256.bmp" "GOLD.bmp" "lennagrey.bmp" "peppersTMW.bmp")
LOGFILE=../log/results.txt

COMMANDS=(
  './team_project_zut -S $val'
  './team_project_zut -S --step1=row-bwt $val'
  './team_project_zut -S --step1=col-bwt $val'
  './team_project_zut -S --step1=row-bwt --step2=col-bwt $val'
  './team_project_zut -S --step1=col-bwt --step2=row-bwt $val'
  './team_project_zut -S --step1=pred $val'
  './team_project_zut -S --step1=pred --step2=row-bwt $val'
  './team_project_zut -S --step1=pred --step2=col-bwt $val'
  './team_project_zut -S --step1=pred --step2=row-bwt --step3=col-bwt $val'
  './team_project_zut -S --step1=pred --step2=col-bwt --step3=row-bwt $val'
  './team_project_zut -S --step1=row-bwt --step2=pred $val'
  './team_project_zut -S --step1=col-bwt --step2=pred $val'
  './team_project_zut -S --step1=row-bwt --step2=col-bwt --step3=pred $val'
  './team_project_zut -S --step1=col-bwt --step2=row-bwt --step3=pred $val'
)

LABELS=(
  'ORIGINAL'
  'ORIGINAL + ROW-BWT'
  'ORIGINAL + COL-BWT'
  'ORIGINAL + ROW-BWT + COL-BWT'
  'ORIGINAL + COL-BWT + ROW-BWT'
  'PREDICTION'
  'PREDICTION + ROW-BWT'
  'PREDICTION + COL-BWT'
  'PREDICTION + ROW-BWT + COL-BWT'
  'PREDICTION + COL-BWT + ROW-BWT'
  'ROW-BWT + PREDICTION'
  'COL-BWT + PREDICTION'
  'ROW-BWT + COL-BWT + PREDICTION'
  'COL-BWT + ROW-BWT + PREDICTION'
)

function print_header {
  echo "============================"
  echo "<AUTOMATION SCRIPT>"
  echo "output in: [project-root]/log/results.txt"
  echo $'============================\n'
}

IM=${IMAGES2[@]}
cd cmake-build-debug
true > "$LOGFILE"

print_header
for com in "${!COMMANDS[@]}"; do
  echo "${LABELS[$com]}" | tee -a "$LOGFILE"
  for val in ${IM[*]}; do
    eval "${COMMANDS[$com]}" | tee -a $LOGFILE
  done
  echo $'----------------------------\n' | tee -a "$LOGFILE"
done
