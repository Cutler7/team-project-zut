# Team Project ZUT
This program is a part of team project at the university. 
Program reads bitmap from file and execute on the data many transformations
giving on output such data as: histograms, entropy and other indicators
related to data compression. It is used as tool to analise transformations of
digital images compression.

### Compilation
Program can be built with CMake. First download and install library called Eigen 3.3.
It's main dependency of the project. Instructions about installing Eigen you can find
on their [website](http://eigen.tuxfamily.org/).

### Usage
``` 
team_project_zut [OPTIONS] [FILENAME]

-s -S                             write output information to log files
-c -C                             write pixel values to csv
step[1..5] [operation_name]       define steps to process
```
Following transformations are available:
* row-bwt - BWT on image row by row
* col-bwt - BWT on image column by column
* full-bwt - bidirectional BWT: first for rows and next for columns
* diff - transform bitmap to differential coding
* pred - transform bitmap to prediction coding

##### Example
```
./team_project_zut -s --step1=row-bwt --step2=pred GOLD.bmp
```
### Analysis
First at all you have to create virtualenv and install matplotlib 
Python library. Then run main.py script placed in src/analysis directory.

You can run the script with following parameters: 
* hist - show image histograms
* map - draw pixels from csv
