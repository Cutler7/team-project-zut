//
// Created by cutler on 3/10/20.
//

#include <iostream>
#include "Bmp.h"
#include "RunTransformation.h"
#include "GetProgramParams.h"

using namespace std;

RegisteredParams params;
ImageData *source, *result, *bmp;
ImageData *objRefs[NUMBER_OF_STEPS]{};

void clearMemory() {
    delete bmp;
    for (int i = 0; i < NUMBER_OF_STEPS; i++) {
        delete[] objRefs[i];
    }
}

BwtVariant getProperBitAvgMode(const RegisteredParams &params) {
    BwtVariant result = BwtVariant::NONE;
    bool colBwt = false;
    bool rowBwt = false;
    bool fullBwt = false;
    for (int i = 0; i < NUMBER_OF_STEPS; i++) {
        if (params.steps[i] == ImageOperation::COLUMN_BWT) colBwt = true;
        if (params.steps[i] == ImageOperation::ROW_BWT) rowBwt = true;
        if (params.steps[i] == ImageOperation::FULL_BWT) fullBwt = true;
    }
    if (rowBwt && !colBwt && !fullBwt) result = BwtVariant::ROW;
    if (colBwt && !rowBwt && !fullBwt) result = BwtVariant::COL;
    if (fullBwt || (rowBwt && colBwt)) result = BwtVariant::FULL;
    return result;
}

int main(int argc, char *argv[]) {
    params = getProgramParams(argc, argv);
    ImageData *bmp = new Bmp("../assets/" + params.fileName, params);
    source = bmp;

    for (int i = 0; i < NUMBER_OF_STEPS; i++) {
        result = runTransformation(source, result, params.steps[i], params);
        objRefs[i] = result;
        source = result;
    }

    BwtVariant mode = getProperBitAvgMode(params);
    result->getBitAvg(mode);

//    clearMemory();
//    cout << "-------------------------" << endl;
    return 0;
}
